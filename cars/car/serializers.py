from rest_framework import serializers
from .models import *

class CountrySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Country
        fields = ('id', 'country')


class CarSerializer(serializers.HyperlinkedModelSerializer):

 #   number_object = serializers.SerializerMethodField('get_id')

    class Meta:
        model = Car
        fields = ('id', 'id_country', 'brand', 'model', 'year')


class PartsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Parts
        fields = ('id', 'id_car', 'id_country', 'name', 'count')
