from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import *
from .serializers import *
from rest_framework import viewsets
from django.http import HttpResponse

# Create your views here.
class CarViewSet(viewsets.ModelViewSet):
    queryset = Car.objects.all()
    def get_serializer(self, *args, **kwargs):
        return CarSerializer

class CountryViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    def get_serializer(self, *args, **kwargs):
        return CountrySerializer


class PartsViewSet(viewsets.ModelViewSet):
    queryset = Parts.objects.all()
    def get_serializer(self, *args, **kwargs):
        return PartsSerializer