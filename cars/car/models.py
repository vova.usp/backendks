from django.db import models

# Create your models here.

class Country(models.Model):
    countries = [('JPN', 'Япония'), ('USA', 'США'), ('CHN', 'Китай'), ('KOR', 'Корея'), ('ENG', 'Англия'),
                 ('GER', 'Германия'), ('FRA', 'Франция'), ('SWE', 'Швеция'), ('ESP', 'Испания'),
                 ('CZE','Чехия'), ('ROU', 'Румыния'), ('RUS', 'Россия')]
    country = models.CharField(verbose_name='Страна приозведителя', max_length=10, choices=countries, default='JPN')

    def __str__(self):
        return self.country

    class Meta:
        verbose_name = "Название страны"
        verbose_name_plural = "Названия стран"

class Car(models.Model):
    car_brands = [('TMC', 'Toyota'), ('FMC', 'FORD'), ('HVL', 'Haval'), ('KIA', 'Kia'), ('JLR', 'Land Rover'),
                  ('VAG', 'Volkswagen'), ('FNV', 'Ferrari'), ('RNO', 'Renault'), ('VLV', 'Volvo'), ('SAT', 'Seat'),
                  ('SKD', 'Skoda'), ('DAC', 'Dacia'), ('TGZ', 'TaGAZ')]
    id_country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True, related_name='id_country')
    brand = models.CharField(verbose_name='Марка авто', max_length=10, choices=car_brands, default='TMC')
    model = models.CharField(verbose_name='Модель авто', max_length=10, null=True)
    year = models.IntegerField(verbose_name='Год выпуска', null=True)

    def __str__(self):
        return self.brand

    class Meta:
        verbose_name = "Название авто"
        verbose_name_plural = "Названия авто"

class Parts(models.Model):
    id_car = models.ForeignKey(Car, verbose_name='Код автомобиля', on_delete=models.CASCADE)
    id_country = models.ForeignKey(Country, verbose_name="Код страны производителя", on_delete=models.PROTECT)
    name = models.CharField(verbose_name='Наименование запчасти', max_length=100)
    count = models.IntegerField(verbose_name='Количество')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Запчасть"
        verbose_name_plural = "Запчасти"
