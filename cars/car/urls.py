from django.urls import path, include
from .views import *
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views

router = DefaultRouter()

router.register(r'country', CountryViewSet, basename="country")
router.register(r'car', CarViewSet, basename="car")
router.register(r'parts', PartsViewSet, basename="parts")

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api-token-auth/', views.obtain_auth_token),

]