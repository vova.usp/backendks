from django.contrib import admin
from .models import Car,Country,Parts
# Register your models here.
admin.site.register(Country)
admin.site.register(Car)
admin.site.register(Parts)